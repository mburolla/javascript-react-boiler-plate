import React from 'react'
import { SidebarNav } from './SidebarNav'
import { SidebarOrder } from './SidebarOrder'
import './SidebarBody.scss'

export const SidebarBody = () => {
    return (
        <div className='SidebarBody'>
            <SidebarNav />
            <hr/>
            Your cart is empty.
            <hr/>
            <SidebarOrder />
        </div>
    )
}
