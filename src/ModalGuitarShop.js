import React from 'react'
import Modal from 'react-bootstrap/Modal'
import { appName } from './Util/App.Config'
import Button from 'react-bootstrap/Button'

export const ModalGuitarShop = ({showModal, setShowModal, message}) => {
    return (
        <div>
            <Modal show={showModal} onHide={() => setShowModal(false)}>
            <Modal.Header closeButton>
                <Modal.Title>{ appName }</Modal.Title>
            </Modal.Header>
            <Modal.Body>{message}</Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={() => setShowModal(false)}>Close</Button>
            </Modal.Footer>
            </Modal>
        </div>
    )
}
