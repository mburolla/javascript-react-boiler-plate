import React from 'react'
import './UsersTable.scss'

export const UsersTable = ({users}) => {
    return (
        <div className='UsersTable'>
            <table>
                <thead />
                <tbody>
                    { 
                        users.map(i => (
                            <tr key={i.id}>
                                <td>{i.id}</td>
                                <td>{i.name}</td>
                                <td>{i.email}</td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
        </div>
    )
}
