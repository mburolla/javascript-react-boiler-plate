import React from 'react'
import { Link } from 'react-router-dom'
import { useRecoilState } from 'recoil'
import { showSideDrawer } from './Util/Atoms'

import './SidebarNav.scss'

export const SidebarNav = () => {
    let [, setShowSideDrawer] = useRecoilState(showSideDrawer)

    return (
        <div className='SidebarNav'>
            <div className='SidebarNav_LinkContainer'>
                <Link 
                    to='/'
                    className='SidebarNav_Link'
                    onClick={() => setShowSideDrawer(false)} 
                    style={{backgroundImage: `url(${require('./icons/home-icon.png')})`}}>Home
                </Link>
            </div>
            <div className='SidebarNav_LinkContainer'>
                <Link 
                    to='/guitars'
                    className='SidebarNav_Link'
                    style={{backgroundImage: `url(${require('./icons/home-icon.png')})`}}
                    onClick={() => setShowSideDrawer(false)}>Guitar
                </Link>
            </div>
            <div className='SidebarNav_LinkContainer'>
                <Link 
                    to='/about'
                    className='SidebarNav_Link'
                    style={{backgroundImage: `url(${require('./icons/home-icon.png')})`}}
                    onClick={() => setShowSideDrawer(false)}>About
                </Link>
            </div>
        </div>
    )
}
