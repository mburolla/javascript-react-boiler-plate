import React from 'react'
import { useRecoilState } from 'recoil'
import { appName } from './Util/App.Config'
import { showSideDrawer } from './Util/Atoms'
import './Header.scss'

export const Header = () => {
    let [, setShowSideDrawer] = useRecoilState(showSideDrawer)

    return (
        <div className='Header'>
            <div className='Header_Hamburger'>
                <img 
                    onClick={() => setShowSideDrawer(true)} 
                    height='40px' 
                    width='40px' 
                    src='./icons/hamburger-icon.png' 
                    alt='menu'>
                </img>
            </div>
            <div className='Header_Title Product_Font'>
                { appName }
            </div>
            <div className='Header_Cart' onClick={() => setShowSideDrawer(true)}>
                <img width='40px' src='./icons/shopping-cart-icon.png' alt='shopping cart'/>
            </div>
        </div>
    )
}
