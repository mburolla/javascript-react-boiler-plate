import React from 'react'
import { useQuery } from 'react-query'
import { UsersTable } from '../UsersTable'
import { getUser, getUsers } from '../Util/ApiProxy'
import './AboutPage.scss'

export const AboutPage = () => {
    const userQuery = useQuery(`users/1`, async () => await getUser())
    const usersQuery = useQuery(`users`, async () => await getUsers())

    return (
        <div className='AboutPage'>
            <div className='Center_Content'>
                <br />
                <br />
                <div className='Center_Item'>
                    About Page
                </div>
                <br />
                <div className='Center_Item'>
                { 
                    userQuery.isFetched && 
                    userQuery.data.name 
                }
                </div>
                <br />
                <div className='Center_Item'>
                { 
                    usersQuery.isFetched && 
                    <UsersTable users={usersQuery.data} />
                }    
                </div>
                <br />
            </div>
        </div>
  )
}
