
import Image from 'react-bootstrap/Image'
import './HomePage.scss'

export const HomePage = () => {
    return (
        <div className='HomePage'>
            <div className='Center_Content'>
                <br />
                <div className='Center_Item'>
                    <Image width='500px' rounded={true} fluid={true} src='guitar-1.PNG' alt='band' />
                </div>
                <br />
                <br />
            </div>
        </div>
  )
}
