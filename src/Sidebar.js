import React from 'react'
import { useRecoilState } from 'recoil'
import { appName } from './Util/App.Config'
import { SidebarBody } from './SidebarBody'
import { showSideDrawer } from './Util/Atoms'
import Offcanvas from 'react-bootstrap/Offcanvas'
import './Sidebar.scss'

export const Sidebar = () => {
    let [showTheSideDrawer, setShowSideDrawer] = useRecoilState(showSideDrawer)

    return (
        <div className='Sidebar'>
            <Offcanvas show={showTheSideDrawer} onHide={() => setShowSideDrawer(false)}>
                <Offcanvas.Header className="Sidebar_Offcanvas_Header" closeButton>
                <Offcanvas.Title>
                    <div className="Sidebar_Offcanvas_Title Product_Font">
                        { appName }
                    </div>
                </Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body className="Sidebar_Offcanvas_Body"  >   
                    <SidebarBody />
                </Offcanvas.Body>
            </Offcanvas> 
        </div>
    )
}
