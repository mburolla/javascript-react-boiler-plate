import React, { useState } from 'react'
import { Button } from 'react-bootstrap'
import { ModalGuitarShop } from './ModalGuitarShop'
import './SidebarOrder.scss'

export const SidebarOrder = () => {
    let [showModal, setShowModal] = useState(false)

    const onHandleCheckout = () => {
        setShowModal(true)
    }

    return (
        <div className='SidebarOrder'>
            <div className='SidebarOrder_Checkout'>
                <Button variant='primary' 
                    onClick={ () => onHandleCheckout() }>Checkout
                </Button>
                <ModalGuitarShop
                    showModal={showModal} 
                    setShowModal={() => setShowModal(false)}
                    message={'Thank you for your order!'} 
                />
            </div>
        </div>
    )
}
