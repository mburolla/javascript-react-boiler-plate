import { Footer } from './Footer'
import { Header } from './Header'
import { Sidebar } from './Sidebar'
import { useEffect } from 'react'
import { HomePage } from './Pages/HomePage'
import { AboutPage } from './Pages/AboutPage'
import { GuitarPage } from './Pages/GuitarPage'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { initLocalStorageProxy } from './Util/LocalStorageProxy'
import './App.scss';

function App() {

  useEffect(() => {
    initLocalStorageProxy()
  }, [])

  return (
    <div className="App">
        <Header />
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<HomePage />}/>
            <Route path="about" element={<AboutPage />}/>
            <Route path="guitars" element={<GuitarPage />}/>
          </Routes>
          <Sidebar />
        </BrowserRouter>
        <Footer />
    </div>
  );
}

export default App;
