import App from './App'
import React from 'react'
import { RecoilRoot } from 'recoil'
import ReactDOM from 'react-dom/client'
import { QueryClient, QueryClientProvider } from 'react-query'
import 'bootstrap/dist/css/bootstrap.min.css'
import './shared-style.scss'

const queryClient = new QueryClient()
const root = ReactDOM.createRoot(document.getElementById('root'))

root.render(
  <RecoilRoot >
    <QueryClientProvider client={queryClient}> 
      <React.StrictMode>
        <App />
      </React.StrictMode>
    </QueryClientProvider>
  </RecoilRoot>
)
