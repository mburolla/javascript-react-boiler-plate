//
// File: SessionStorageProxy.js
// Auth: Martin Burolla
// Date: 10/19/2022
// Desc: The one and only interface into the browser's session storage.
//

const SS_GUITAR_STORE_KEY = 'Guitar_Store_V1'

export const iniSessionStorageProxy = () => {
    const orders = JSON.parse(sessionStorage.getItem(SS_GUITAR_STORE_KEY))
    if (orders == null) {
        sessionStorage.setItem(SS_GUITAR_STORE_KEY, JSON.stringify([]))
    } 
}

export const getSessionId = () => {
    let sessionId = sessionStorage.getItem(SS_GUITAR_STORE_KEY)
    if (sessionId == null) {
        sessionStorage.setItem(SS_GUITAR_STORE_KEY, JSON.stringify([]))
        sessionId = sessionStorage.getItem(SS_GUITAR_STORE_KEY)
    }
    return JSON.parse(sessionId)
}

export const setSessionId = (sessionId) => {
    sessionStorage.setItem(SS_GUITAR_STORE_KEY, JSON.stringify(sessionId))
}
