import { atom } from 'recoil'

export const showSideDrawer = atom({
    key: 'showSideDrawer',
    default: false 
})
