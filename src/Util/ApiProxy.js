//
// File: ApiProxy.js
// Auth: Martin Burolla
// Date: 10/19/2022
// Desc: The one and only interface to the web API.
//       Consume the ApiProxy: import * as apiProxy from './Util/ApiProxy'
//

import axios from 'axios'

const DESC_1_ENDPOINT = ''
const GET_USER = '/users/1'
const GET_USERS = '/users'
const BASE_URL = 'https://jsonplaceholder.typicode.com/'

const api = axios.create({ baseURL: BASE_URL })

export const getData1 = async (reqBody) => {
    const body = { }
    const res = await api.post(DESC_1_ENDPOINT, body)
    return res.data
}

export const getUser = async () => {
    const res = await api.get(GET_USER)
    return res.data
}

export const getUsers = async () => {
    const res = await api.get(GET_USERS)
    return res.data
}
