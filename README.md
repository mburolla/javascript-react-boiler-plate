# JavaScript React Guitar Store

A boilerplate project that demonstrates the following:
- React-Bootstrap (Style)
- Recoil (Global state management)
- React-Query/Axios (Web API calls & cache)
- Sass (style)
- React Router DOM (Deep linking)

![](./docs/screen-shot.png)

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Start app: `npm start`

# Deployment
- Run tests: `npm test`
- Build & deploy to S3 bucket: `deploy\build-deploy.bat`

